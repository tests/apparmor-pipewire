#!/bin/sh

# run-test-in-systemd.sh — run a LAVA test as a systemd unit
#
# Copyright © 2015, 2016, 2017 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

usage='
Usage:
  run-test-in-systemd [OPTIONS...] [--] COMMAND [ARGS...]
                        Run COMMAND ARGS as a systemd unit
Options:
  --system|--user=UID   Run as a system or user service
                        (default: current user, or --system if run as root)
  --name=NAME           Use NAME as the test-case name, escaping special
                        characters (default: COMMAND ARGS)
  --basename            Use the non-directory part of COMMAND as the test-case
                        name for LAVA, stripping extensions .pl, .py and/or
                        .sh and escaping special characters
  --timeout=EXPR        Set a timeout (systemd syntax is allowed, e.g. 300,
                        300s, 5min, 300000ms are equivalent; default 15min)
  --chdir=DIRNAME       Change current working directory to DIRNAME
  --exit-status         Exit with the status of COMMAND
  --no-lava             Do not integrate with LAVA (implies --exit-status)
  --debug               Show journal messages
  --full-debug          Show full journal logs
'

set -e

as_target_user=
debug=
full_debug=
journalctl="journalctl --no-pager --full"
lava_runes=1
name=""
progname="$(basename "$0")"
systemctl="systemctl"
target_user=
time_adverb=""
timeout=15min
uid="$(id -u)"
use_basename=
use_exit_status=
working_dir="$(pwd)"

debug () {
    [ -z "${debug}" ] || echo "# ${progname}: $*" >&2
}

die () {
    echo "${progname}: ERROR: $*" >&2
    exit 1
}

systemd_escape_argument () {
    # Escape an argument for ExecStart
    # - escape % as %%, $ as $$ and \ as \\
    # - escape single and double quotes
    # - escape control characters, delete and non-ASCII like \xff
    # I can't work out how to do the latter in sh so I'm resorting to Perl
    perl -e '$_ = $ARGV[0];
        s/([\\%\$])/$1$1/g;
        s/([\x22\x27])/\\$1/g;
        s/([^\x20-\x7e])/sprintf(q(\\x%02x), ord $1)/ge;
        print qq("$_");
    ' -- "$1"
}

if [ "x${uid}" != x0 ]
then
    target_user="${uid}"
    journalctl="sudo ${journalctl}"
fi

if which busybox >/dev/null; then
    time_adverb="busybox time"
elif which time >/dev/null; then
    time_adverb="time"
else
    time_adverb=""
fi

if ! which "lava-test-case" >/dev/null; then
    lava_runes=
    use_exit_status=1
fi

# -o + means stop parsing at the first non-option argument, so we can do
# things like: run-test-in-systemd sh -c 'echo hello'
args="$(getopt \
    -o '+h' \
    -l "basename,chdir:,debug,exit-status,full-debug,help,name:,no-lava,system,timeout:,user:" \
    -n "${progname}" -- "$@")"
eval set -- "${args}"

while [ "$#" -gt 0 ]; do
    case "$1" in
        (--basename)
            use_basename=1
            shift
            ;;
        (--debug)
            debug=1
            shift
            ;;
        (--exit-status)
            use_exit_status=1
            shift
            ;;
	(--full-debug)
	    full_debug=1
	    shift
	    ;;
        (--help|-h)
            echo "${usage}"
            exit 0
            ;;
        (--name)
            name="$2"
            shift 2
            ;;
        (--no-lava)
            lava_runes=
            use_exit_status=1
            shift
            ;;
        (--system)
            target_user=
            as_uid=
            shift
            ;;
        (--timeout)
            timeout="$2"
            shift 2
            ;;
        (--user)
            # $2 is a numeric uid or a username
            target_user="$(getent passwd "$2")" || die "user not found: $2"
            # accept a username or a uid, convert to a uid
            target_user="$(echo "${target_user}" | cut -d: -f3)"
            shift 2
            ;;
	(--chdir)
	    working_dir="$2"
	    shift 2
	    ;;
        (--)
            shift
            break
            ;;
        (*)
            break
            ;;
    esac
done

[ "$#" -gt 0 ] || die "a command is required"

if [ -n "${use_basename}" ]; then
    name="$1"
    name="${name##*/}"
    name="${name%.pl}"
    name="${name%.py}"
    name="${name%.sh}"
elif [ -z "${name}" ]; then
    name="$*"
fi

my_dir="$(dirname "$0")"
my_dir="$(cd "$my_dir" && pwd)"
tee_exec="${my_dir}/tee-exec"

name="$(echo -n "${name}" | tr -c -s 'A-Za-z0-9---_' '_' | head -c 220)"
service="generated-test-case-${name}.service"

if [ -n "${target_user}" ]; then
    debug "running test as a user service for uid ${target_user}"

    # We've probably been invoked in some stack of sudo and su
    # commands, so our login environment is probably nonsense;
    # fix it so we can contact systemd.
    #
    # This environment is only used to contact systemd, so it is
    # safe for it to be rather minimal. The actual test runs in
    # the environment provided by systemd, as documented in systemd.exec(5)
    # and potentially augmented by the SetEnvironment() call.
    #
    # System services can rely on having PATH and LANG.
    #
    # User services can additionally rely on SHELL, USER, LOGNAME, HOME,
    # MANAGERPID, XDG_RUNTIME_DIR.
    #
    # Non-systemd components in Apertis currently set DISPLAY and
    # DBUS_SESSION_BUS_ADDRESS, although the former may be lost in the
    # migration to Wayland and the latter is redundant with XDG_RUNTIME_DIR
    # (our three supported D-Bus implementations - GDBus, libdbus and sd-bus -
    # all default to unix:path=$XDG_RUNTIME_DIR/bus).
    XDG_RUNTIME_DIR="/run/user/${target_user}"
    export XDG_RUNTIME_DIR
    unset DBUS_SESSION_BUS_ADDRESS

    if ! [ -S "${XDG_RUNTIME_DIR}/bus" ]; then
        die "${XDG_RUNTIME_DIR}/bus does not exist, cannot proceed"
    fi

    if [ "${uid}" != "${target_user}" ]; then
        as_target_user="sudo -u #${target_user} env XDG_RUNTIME_DIR=${XDG_RUNTIME_DIR}"
    fi

    filename="${XDG_RUNTIME_DIR}/systemd/user"
    debug "creating ${filename}"
    ${as_target_user} install -m 700 -d "${filename}"
    filename="${filename}/${service}"
    systemctl="systemctl --user"
else
    debug "running test as a system service"
    filename="/run/systemd/system/${service}"
    if [ "$(id -ru)" != 0 ]; then
        as_target_user="sudo"
    fi
fi

systemctl="${as_target_user} ${systemctl}"

debug "getting Journal cursor"
cursor="$(${journalctl} --show-cursor -n 0 -o cat)"
cursor="${cursor#-- cursor: }"
debug "Journal cursor: ${cursor}"

${as_target_user} rm -f "${filename}.tmp"
${as_target_user} rm -f "${filename}"

user_log_dir="$(${as_target_user} mktemp -d)"
# We write the output to a fifo (named pipe) so that we can watch it in
# real-time, below. We start watching it before we start the test.
${as_target_user} mkfifo "${user_log_dir}/stdout.fifo"
exec_start="${tee_exec} ${user_log_dir}/stdout.fifo"

while [ "$#" -gt 0 ]; do
    exec_start="${exec_start} $(systemd_escape_argument "$1")"
    shift
done

debug "creating ${filename} to run ${exec_start}"

# Not using systemd-run because it doesn't give us control over
# the timeout, and systemd is better at enforcing those than LAVA is.
# Use tee to get a sudo'able pseudo-redirection.
${as_target_user} tee ${filename}.tmp >/dev/null <<EOF
[Service]
Type=oneshot
TimeoutSec=${timeout}
WorkingDirectory=${working_dir}
ExecStart=${exec_start}
StandardOutput=journal
StandardError=inherit
SyslogIdentifier=${service}
Restart=no
EOF
${as_target_user} mv "${filename}.tmp" "${filename}"

if [ -n "${debug}" ]; then
    debug ".service file:"
    sed -e "s/^/# ${progname}:  /" < ${filename} >&2
fi

debug "reloading via ${systemctl} daemon-reload"
${systemctl} daemon-reload
${systemctl} stop ${service} || :

debug "starting test"

# Watch the output in real time, both for developers' benefit and so that
# LAVA does not think "it has become inactive" and kill us.
${as_target_user} cat "${user_log_dir}/stdout.fifo" &
cat_fifo_pid="$!"

if ${time_adverb} ${systemctl} start ${service}; then
    debug "test passed"
    result=pass
    exit_status="0"
else
    exit_status="$?"
    if [ "${exit_status}" = 77 ]; then
        debug "test skipped: $?"
        result=skip
    else
        debug "test failed: $?"
        result=fail
    fi
fi

debug "killing any leftover test processes"
${systemctl} stop ${service} || :

debug "waiting for end-of-file on command output..."
wait "$cat_fifo_pid" || result=fail
rm -fr "${user_log_dir}" || result=fail

if [ -n "${lava_runes}" ]; then
    "lava-test-case" "${name}" --result "${result}"
fi

# Show journal logs if there is an error and any of the `debug` flag was passed,
# otherwise suggest to use debug flags to show more information.
if [ "${exit_status}" != 0 ]; then
    if [ -n "${full_debug}" ]; then
	debug "Showing full logs (--full-debug passed)"
	${journalctl} -o verbose -b -0 --after-cursor "${cursor}" || :
    elif [ -n "${debug}" ]; then
	debug "Showing logs (--debug passed)"
	${journalctl} -b -0 --after-cursor "${cursor}" || :
    else
	echo "NOTE: Run command with the --debug or --full-debug option to enable journal logs"
    fi
fi

${as_target_user} rm -f "${filename}.tmp"
${as_target_user} rm -f "${filename}"

if [ -n "${use_exit_status}" ]; then
    exit "${exit_status}"
fi

# vim:set sw=4 sts=4 et:
