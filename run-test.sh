#!/bin/sh
# vim: tw=0

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
. "${TESTDIR}/config.sh"

# We want to use the pre-existing session bus.
export LAUNCH_DBUS="no"

error=0
"${TESTDIR}"/common/run-aa-test "${TESTDIR}"/pipewire.normal.expected "${TESTDIR}"/pipewire || error=$?

if [ $error = 0 ]; then
  echo "# apparmor-pipewire: test passed"
else
  echo "# apparmor-pipewire: test failed"
fi

exit $error
